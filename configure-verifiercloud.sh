# Add this file (or symlink it) as ~/.competition-configure-verifiercloud.sh if you want to execute run collections on a VerifierCloud.

BENCHEXECOPTIONS="$BENCHEXECOPTIONS --read-only-dir / --hidden-dir /home --overlay-dir . --vcloudAdditionalFiles . --vcloudClientHeap 3000 --cgroupAccess ${TESTCOMPOPTION}"
if [ -e /data ]; then
  # If scratch folder 'data' exists, make it read-only to avoid benchexec errors
  # on container creation
  BENCHEXECOPTIONS="$BENCHEXECOPTIONS --read-only-dir /data"
fi
export OPTIONSVERIFY="$BENCHEXECOPTIONS $LIMITS --vcloudPriority HIGH"
export OPTIONSVALIDATE="$BENCHEXECOPTIONS $LIMITSVALIDATION --vcloudPriority URGENT --no-ivy-cache"

