#!/bin/bash

# @title Competition Website: Participating Teams
# @phase Web
# @description creates the table of participating teams for the competition website. Data is fetched from the google sheets. 

source $(dirname "$0")/../configure.sh

echo "<!-- This file is generated. Do not edit manually. -->";

echo "

<h2>Verifiers</h2>

<table>
  <tr style='font-weight: bold;'>
    <td>Tool</td>
    <td>Lang.</td>
    <td>Jury member</td>
    <td>Affiliation</td>
    <td>Archive</td>
    <td>Bechmark definition</td>
    <td>Publication</td>
  </tr>
  <tr>
    <td style='font-size: 80%; font-weight: bold;'>
      Overview
    </td>
    <td style='font-size: 70%;'>
      Chair
    </td>
    <td style='font-size: 70%;'>
      <a href='https://www.sosy-lab.org/~dbeyer/'>Dirk Beyer</a>
    </td>
    <td style='font-size: 70%;'>
      LMU Munich, Germany
    </td>
    <td style='font-size: 70%';>
 
    </td>
    <td style='font-size: 70%';>

    </td>
    <td>
      <!--<a href="https://www.sosy-lab.org/research/prs/2019-04-06_SV-COMP_Dirk.pdf">talk</a>-->
    </td>
  </tr>
";

yq --raw-output ".verifiers | to_entries [] \
    | [.key, .value.name, .value.url, .value.lang, .value.\"jury-member\".name, .value.\"jury-member\".institution, .value.\"jury-member\".country, .value.\"jury-member\".url] \
    | join(\"\t\")" \
    benchmark-defs/category-structure.yml \
  | sort \
  | while IFS=$'\t' read TOOL TOOLNAME TOOLURL TOOLLANG MEMBERNAME MEMBERINST MEMBERCOUNTRY MEMBERURL; do
      echo "  <tr>";
      TOOLFUNC=${TOOL//[2]/two}
      TOOLFUNC=${TOOLFUNC//\-/}
      TOOLARCHIVE="$TOOL.zip"
      TOOLFUNC="${TOOLFUNC,,}$YEAR";
      echo "    <td style='font-size: 80%; font-weight: bold;'>";
      if [ "$TOOLURL" != "" ]; then
        echo "      <a href=\"$TOOLURL\">$TOOLNAME</a>";
      else
        echo "      $TOOLNAME";
      fi
      echo "    </td>";
      echo "    <td style='font-size: 70%;'>";
      echo "      $TOOLLANG";
      echo "    </td>";
      echo "    <td style='font-size: 70%;'>";
      if [ "$MEMBERURL" != "" ]; then
        echo "      <a href='$MEMBERURL'>$MEMBERNAME</a>";
      else
        echo "      $MEMBERNAME";
      fi
      echo "    </td>";
      echo "    <td style='font-size: 70%;'>";
      echo "      $MEMBERINST, $MEMBERCOUNTRY";
      echo "    </td>";
      echo "    <td style='font-size: 70%';>";
      echo "      <a href='https://gitlab.com/sosy-lab/$TARGETSERVER/archives-$YEAR/raw/${TARGETSERVER/-/}${YEAR#??}/$YEAR/$TOOL.zip'>$TOOL.zip</a>";
      echo "    </td>";
      echo "    <td style='font-size: 70%';>";
      echo "      <a href='https://gitlab.com/sosy-lab/$TARGETSERVER/bench-defs/blob/${TARGETSERVER/-/}${YEAR#??}/benchmark-defs/$TOOL.xml'>$TOOL.xml</a>";
      echo "    </td>";
      echo "    <td>";
      echo "      ...";
      echo "    </td>";
      echo "  </tr>";
    done

echo "
</table>
";

echo "

<h2>Validators</h2>

<table>
  <tr style='font-weight: bold;'>
    <td>Tool</td>
    <td>Lang.</td>
    <td>Type</td>
    <td>Contact</td>
    <td>Affiliation</td>
    <td>Bechmark definition</td>
    <td>Publication</td>
  </tr>
";

yq --raw-output ".validators | to_entries [] \
    | [.key, .value.name, .value.url, .value.lang, .value.\"jury-member\".name, .value.\"jury-member\".institution, .value.\"jury-member\".country, .value.\"jury-member\".url] \
    | join(\"\t\")" \
    benchmark-defs/category-structure.yml \
  | sort \
  | while IFS=$'\t' read TOOL TOOLNAME TOOLURL TOOLLANG MEMBERNAME MEMBERINST MEMBERCOUNTRY MEMBERURL; do
      echo "  <tr>";
      TOOLFUNC=${TOOL//[2]/two}
      TOOLFUNC=${TOOLFUNC//\-/}
      TOOLARCHIVE="$TOOL.zip"
      TOOLFUNC="${TOOLFUNC,,}$YEAR";
      echo "    <td style='font-size: 80%; font-weight: bold;'>";
      if [ "$TOOLURL" != "" ]; then
        echo "      <a href=\"$TOOLURL\">$TOOLNAME</a>";
      else
        echo "      $TOOLNAME";
      fi
      echo "    </td>";
      echo "    <td style='font-size: 70%;'>";
      echo "      $TOOLLANG";
      echo "    </td>";
      echo "    <td style='font-size: 70%;'>";
      echo "      $(echo $TOOL | sed -e "s/.*-validate-\(.*\)witnesses/\1/" -e "s/-//")";
      echo "    </td>";
      echo "    <td style='font-size: 70%;'>";
      if [ "$MEMBERURL" != "" ]; then
        echo "      <a href='$MEMBERURL'>$MEMBERNAME</a>";
      else
        echo "      $MEMBERNAME";
      fi
      echo "    </td>";
      echo "    <td style='font-size: 70%;'>";
      echo "      $MEMBERINST, $MEMBERCOUNTRY";
      echo "    </td>";
      echo "    <td style='font-size: 70%';>";
      echo "      <a href='https://gitlab.com/sosy-lab/$TARGETSERVER/bench-defs/blob/${TARGETSERVER/-/}${YEAR#??}/benchmark-defs/$TOOL.xml'>$TOOL.xml</a>";
      echo "    </td>";
      echo "    <td>";
      echo "      ...";
      echo "    </td>";
      echo "  </tr>";
    done

echo "
</table>
";

