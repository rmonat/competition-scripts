#!/bin/bash

# Generate list of issues to initialize the discussion issues per tool in the archives repository.


echo "title	description";
yq --raw-output ".verifiers | to_entries [] \
    | [.key, .value.name, .value.url, .value.\"jury-member\".name, .value.\"jury-member\".institution, .value.\"jury-member\".country, .value.\"jury-member\".url] \
    | join(\"\t\")" \
    benchmark-defs/category-structure.yml \
  | sort \
  | while IFS=$'\t' read TOOL TOOLNAME TOOLURL MEMBERNAME MEMBERINST MEMBERCOUNTRY MEMBERURL; do
  echo "$TOOLNAME	\"Project URL: $TOOLURL
  
Jury Member: $MEMBERNAME ($MEMBERINST, $MEMBERCOUNTRY, $MEMBERURL)\"";
done

